/**
 * Created by dima on 28/03/16.
 */

'use strict';
(function() {
    //ui module
    angular.module('app.ui', ['app.ui.routes', 'app.ui.tabs', 'app.ui.fields', 'app.ui.form']);
})();