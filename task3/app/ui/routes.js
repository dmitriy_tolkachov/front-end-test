/**
 * Created by dima on 28/03/16.
 */

angular.module('app.ui.routes',['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/fields');
            $stateProvider
                .state('fields', {
                    url: '/fields',
                    templateUrl: 'app/ui/templates/fields.html'
                })
                .state('form', {
                    url: '/form',
                    templateUrl: 'app/ui/templates/form.html'
                });
        }
    ]);