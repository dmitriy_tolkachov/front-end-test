/**
 * Created by dima on 28/03/16.
 */
(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.ui:fields
     * @restrict A
     * @element ANY
     *
     * @description Insert fields list
     *
     * @Example
     * <div app-fields></div>
     *
     **/
    function fields() {
        return {
            restrict: 'A',
            templateUrl: 'app/ui/fields/fields.tmpl.html',
            replace: true,
            scope: {},
            controller: function($scope, fieldsSvc) {
                //properties
                $scope.fields = undefined; //just to know what properties used in scope
                $scope.filteredFields = undefined;
                $scope.searchValue = undefined;

                //methods
                $scope.toggleField = function(field) {
                    field.isSelected = !field.isSelected;
                };
                $scope.selectAllFields = function() {
                    $scope.fields.forEach(function(field) {
                        field.isSelected = true;
                    });
                };
                $scope.init = function() {
                    fieldsSvc.getFields().then(function(fields) {
                        $scope.fields = fields;
                    });
                };

                //init
                $scope.init();
            }
        };
    }

    angular
        .module('app.ui.fields', [])
        .directive('appFields', fields);
})();