/**
 * Created by dima on 28/03/16.
 */

(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name app.bl:fields
     *
     * @description business logic for fields
     *
     **/
    function fieldsSvc($http, $q) {
        //private
        var DATA_FILE = 'data/fields.json';
        var _fieldsList = [],
            _isDataLoaded = false;

        /**
         * get reference to list of available fields (cached)
         * @param {boolean} forceLoad - if true will load from server anyway
         * @returns {promise|Array}
         */
        var getFields = function(forceLoad) {
            var d = $q.defer();

            //helpers
            var _getCached = function() {
                d.resolve(_fieldsList);
            };
            var _loadFromServer = function() {
                $http.get(DATA_FILE).then(function(res) {
                    _isDataLoaded = true;

                    res.data.forEach(function(item) {
                        var field = new Field(item.name, item.type, item.required, item.minVal, item.maxVal, item.minLength, item.maxLength);
                        _fieldsList.push(field);
                    });
                    //_fieldsList = res.data;
                    d.resolve(_fieldsList);
                }, function() {
                    _isDataLoaded = false;
                    d.reject();
                    throw new Error('fieldsSvc: can`t load initial data', DATA_FILE);
                });
            };

            if (forceLoad) {
                _loadFromServer();
            } else {
                if (_isDataLoaded) {
                    _getCached();
                } else {
                    _loadFromServer();
                }
            }
            return d.promise;
        };

        /**
         *
         * @returns {promise}
         */
        var getSelectedFields = function() {
            var d = $q.defer();

            //helpers
            var _filterFields = function(fields) {
                var res = [];
                fields.forEach(function(field) {
                    if (field.isSelected) {
                        res.push(field);
                    }
                });
                return res;
            };

            if (_isDataLoaded) {
                d.resolve(_filterFields(_fieldsList));
            } else {
                getFields().then(function() {
                    d.resolve(_filterFields(_fieldsList));
                }, function() {
                    d.reject();
                });
            }

            return d.promise;
        };

        //public
        return {
            getFields: getFields,
            getSelectedFields: getSelectedFields
        };
    }

    angular
        .module('app.bl.fields', [])
        .factory('fieldsSvc', fieldsSvc);
})();