/**
 * Created by dima on 28/03/16.
 */

'use strict';
(function() {
    //root module
    angular.module('app',['app.ui', 'app.bl']);
    //initialize angular app
    angular.bootstrap(document, ['app']);
})();