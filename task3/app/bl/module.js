/**
 * Created by dima on 28/03/16.
 */

'use strict';
(function() {
    //business logic module
    angular.module('app.bl', ['app.bl.fields']);
})();