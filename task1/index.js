/**
 * Created by dima on 27/03/16.
 */

'use strict';

/**
 * find specific properties in array of objects
 * @param {Array} data - array of objects
 * @param {String} bindingPath - binding path to specific property in data array that should be rendered
 *
 * @description: logic separated into 2 functions - first for preparing data in correct format, second - for manipulating with DOM
 *
 * @returns {string[]}
 */
var processList = function(data, bindingPath) {
    if (!data || !data.length || !bindingPath) {
        throw new Error('processList: input data not valid', data, bindingPath);
    }

    //var start = new Date().getTime();
    var res = [];
    var bindingPathArr = bindingPath.split('.');

    if (!bindingPathArr || !bindingPathArr) {throw new Error('processList: can`t parse binding path.', bindingPath); }
    //console.log(bindingPathArr);
    for (var i = 0, len = data.length; i < len; i++) {
        var item = data[i];

        for (var j = 0, jLen = bindingPathArr.length; j < jLen; j++) {
            var val = item[bindingPathArr[j]];
            if (!val) {
                console.log('skipping item %o', item);
                break;
            }

            item = val;
            if (j === (jLen - 1)) {
                res.push(val);
            }
        }
    }
    //console.log('data %o, %s', data, bindingPath);
    //var end = new Date().getTime();
    //var time = end - start;
    //console.log('execution time %d', time);

    return res;
};

/**
 * render values in the DOM list
 * @param {Array} data - array of values to be rendered
 *
 * test: renderValues(['a', 'b', 'c']);
 */
var renderValues = function(data) {
    if (!data || !data.length) {
        throw new Error('renderValues: input data not valid.', data);
    }

    const OUTPUT_SELECTOR = 'output';

    var $array = document.getElementById(OUTPUT_SELECTOR);

    for (var i = 0; i < data.length; i++) {
        var $listEl = document.createElement('li');
        $listEl.appendChild(document.createTextNode(data[i]));
        $array.appendChild($listEl);
    }
};

/**
 * load file with data
 * @param {String} path
 * @param {Function} onOk
 * @param {Function} onError
 */
var loadData = function(path, onOk, onError) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                if (onOk)
                    onOk(JSON.parse(xhr.responseText));
            } else {
                if (onError)
                    onError(xhr);
            }
        }
    };
    xhr.open("GET", path, true);
    xhr.send();
};

//ENTRY POINT: on document ready
(function(){
    loadData('api-response.json', function(response) {
        var data = window.data = response.data,
            bindingPath = window.bindingPath = response.bindingPath;

        var preparedData = processList(data, bindingPath);

        renderValues(preparedData);
    }, function(err) {
       console.log(err);
    });
})();