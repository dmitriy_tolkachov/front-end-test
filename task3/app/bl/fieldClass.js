/**
 * Created by dima on 28/03/16.
 */

'use strict';

/**
 * field entity
 * @param {String} name
 * @param {FIELD_TYPES} type
 * @constructor
 */
var Field = function(name, type, required, minVal, maxVal, minLength, maxLength) {
    this.id = this.generateId();

    this.name = name;
    this.type = type;
    this.value = '';

    //state
    this.isSelected = false;

    //validations
    this.required = required ? true: false;
    this.minVal = minVal;
    this.maxVal = maxVal;
    this.minLength = minLength;
    this.maxLength = maxLength;
};

/**
 * check if field value is valid or not
 * @returns {Boolean}
 */
Field.prototype.validate = function() {
    var validation = new ValidationError(true);

    if (this.required && !this.value) {
        validation.details.push(new ValidationError(false, this.name + ' required.'))
        validation.result = false;
    }

    if (this.type === FIELD_TYPES.number && this.minVal != undefined && this.value < this.minVal) {
        validation.details.push(new ValidationError(false, this.name + ' should be more than ' + this.minVal));
        validation.result = false;
    }
    if (this.type === FIELD_TYPES.number && this.maxVal != undefined && this.value > this.maxVal) {
        validation.details.push(new ValidationError(false, this.name + ' should be less than ' + this.maxVal));
        validation.result = false;
    }

    if (this.type === FIELD_TYPES.text && this.minLength != undefined && this.value.length < this.minLength) {
        validation.details.push(new ValidationError(false, this.name + ' should be longer than ' + this.minLength));
        validation.result = false;
    }
    if (this.type === FIELD_TYPES.text && this.maxLength != undefined && this.value.length > this.maxLength) {
        validation.details.push(new ValidationError(false, this.name + ' should be shorter than ' + this.maxLength));
        validation.result = false;
    }

    return validation;
};

/**
 * prepare field output
 * @returns {{}}
 */
Field.prototype.getOutput = function() {
    var output = {};
    output[this.name.replace(/ /g,"_")] = this.value;
    return output;
};
/**
 * generate field`s id
 * @returns {string}
 */
Field.prototype.generateId = function() {
    var s4 = function() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};