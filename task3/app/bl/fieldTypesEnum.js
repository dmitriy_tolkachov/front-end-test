/**
 * Created by dima on 28/03/16.
 */

'use strict';

var FIELD_TYPES = {
    text: 'text',
    date: 'date',
    number: 'number'
    //money: 'money',
    //select: 'drop_down'
};