/**
 * Created by dima on 28/03/16.
 */
(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.ui:form
     * @restrict A
     * @element ANY
     *
     * @description Insert form based on selected fields
     *
     * @Example
     * <div app-form></div>
     *
     **/
    function form() {
        return {
            restrict: 'A',
            templateUrl: 'app/ui/form/form.tmpl.html',
            replace: true,
            scope: {},
            controller: function($scope, fieldsSvc) {
                //properties
                $scope.fields = undefined;
                $scope.output = [];
                $scope.errors = [];

                //methods
                $scope.init = function() {
                    fieldsSvc.getSelectedFields().then(function(fields) {
                        $scope.fields = fields;
                    });
                };
                $scope.validate = function() {
                    $scope.fields.forEach(function(field) {
                        var validation = field.validate();

                        if (!validation.result) {
                            $scope.errors.push(validation);
                        }
                    });
                };
                $scope.submitForm = function() {
                    $scope.output = [];
                    $scope.errors = [];

                    $scope.validate();

                    //prepare output if no errors
                    if (!$scope.errors.length) {
                        $scope.fields.forEach(function(field) {
                            $scope.output.push(field.getOutput());
                        });
                    }
                };

                //init
                $scope.init();
            }
        };
    }

    angular
        .module('app.ui.form', [])
        .directive('appForm', form);
})();