# README #

### task 1 ###

* open task1/index.html
* open task1/index.js in text editor to see implementation

### task 2 ###

* open task2/index.html
* open console to see results
* open task2/index.js in text editor to see implementation

### task 3 ###

* open command line and go to task3 folder
* run 'npm install'
* open task3/index.html in browser

### TODO task 3 ###
* add building process support (grunt|gulp)
* add css preprocessor (sass)
* add template engine (jade)

