/**
 * Created by dima on 28/03/16.
 */

/**
 * write into console array in reversed order
 * @param {Array} data
 */
function A (data) {
    console.log(data.reverse().join(', '));
}


var data = [1,2,3,4,5];
//var data = ['a','b','c','d','e'];
A(data);
console.log('------');
A(data);

