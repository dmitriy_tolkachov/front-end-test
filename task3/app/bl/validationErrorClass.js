/**
 * Created by dima on 29/03/16.
 */

/**
 * validation error entity
 * @param {Boolean} res
 * @param {String} msg
 * @constructor
 */
var ValidationError = function(res, msg) {
    this.result = res ? true : false;
    this.msg = msg;
    this.details = [];
};