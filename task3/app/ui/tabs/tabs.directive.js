/**
 * Created by dima on 28/03/16.
 */
(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.ui:tabs
     * @restrict A
     * @element ANY
     *
     * @description Insert tabs
     *
     * @Example
     * <div app-tabs></div>
     *
     **/
    function tabs() {
        return {
            restrict: 'A',
            templateUrl: 'app/ui/tabs/tabs.tmpl.html',
            transclude: true,
            replace: true,
            scope: {
                activeTab: '@'
            },
            controller: function($scope) {
                //properties
                $scope.tabs = [
                    {
                        id: 'tab0',
                        name: 'Fields',
                        isSelected: true,
                        link: 'fields'
                    },
                    {
                        id: 'tab1',
                        name: 'Form',
                        isSelected: false,
                        link: 'form'
                    }
                ];

                //methods
                $scope.init = function() {
                     if ($scope.activeTab) {
                         $scope.tabs.forEach(function(tab){
                            tab.isSelected = (tab.id === $scope.activeTab) ? true : false;
                         });
                     } else {
                         //select first tab by default
                         $scope.tabs.forEach(function(tab, index){
                             tab.isSelected = (index === 0) ? true : false;
                         });
                     }
                };

                //init
                $scope.init();
            }
        };
    }

    angular
        .module('app.ui.tabs', [])
        .directive('appTabs', tabs);
})();